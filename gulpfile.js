'use strict';

var gulp = require('gulp'),
    watch = require('gulp-watch'),
    prefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    rigger = require('gulp-rigger'),
    cssmin = require('gulp-minify-css'),
    concat = require('gulp-concat'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    rimraf = require('rimraf'),
    plumber = require('gulp-plumber'),
    extender = require('gulp-html-extend'),
    browserSync = require("browser-sync"),
    reload = browserSync.reload;

var path = {
    build: {
        html: 'build/',
        js: 'build/assets/js/',
        css: 'build/assets/css/',
        img: 'build/assets/images/'
    },
    src: {
        html: 'src/html/*.html',
        js: 'src/js/**/*.js',
        js_vendor: 'src/vendor/vendor.js',
        css_vendor: 'src/vendor/vendor.css',
        style: 'src/css/**/*.scss',
        img: 'src/images/**/*.*'
    },
    watch: {
        html: 'src/html/**/*.html',
        js: 'src/js/**/*.js',
        style: 'src/css/**/*.scss',
        img: 'src/images/**/*.*'
    },
    clean: './assets'
};

var config = {
    //proxy: 'clinical',
    //tunnel: false
     server: {
     baseDir: "./build"
     },

     host: 'localhost',
     port: 9000
};

gulp.task('build:html', function () {
    gulp.src(path.src.html)
        .pipe(plumber())
        .pipe(extender())
        .pipe(gulp.dest(path.build.html))
        .pipe(reload({stream: true}));
});


// ========== Build vendor ============
gulp.task('build:vendor', function () {
    gulp.src(path.src.js_vendor)
        .pipe(plumber())
        .pipe(rigger())
        .pipe(uglify())
        .pipe(gulp.dest(path.build.js));

    gulp.src(path.src.css_vendor)
        .pipe(plumber())
        .pipe(cssmin())
        .pipe(gulp.dest(path.build.css));
});

// ============== CSS =================
gulp.task('build:css', function () {
    gulp.src(path.src.style)
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(prefixer())
        .pipe(concat('main.css'))
        .pipe(cssmin())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.build.css))
        .pipe(reload({stream: true}));
});


// ============== IMAGES =================
gulp.task('build:image', function () {
    gulp.src(path.src.img) //Выберем наши картинки
        .pipe(plumber())
        .pipe(imagemin({ //Сожмем их
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()],
            interlaced: true
        }))
        .pipe(gulp.dest(path.build.img)) //И бросим в build
        .pipe(reload({stream: true}));
});


// ============== BUILD ALL =================
gulp.task('build', [
    'build:html',
    'build:vendor',
    'build:css',
    'build:image'
]);


// ============== WATCH =================
gulp.task('watch', function () {

    watch([path.watch.html], function (event, cb) {
        gulp.start('build:html');
    });

    watch([path.watch.style], function (event, cb) {
        gulp.start('build:css');
    });
/*    watch([path.watch.js], function (event, cb) {
        gulp.start('build:js');
    });*/
    watch([path.src.js_vendor, path.src.css_vendor], function (event, cb) {
        gulp.start('build:vendor');
    });
    watch([path.watch.img], function (event, cb) {
        gulp.start('build:image');
    });
});


// ============== WATCH =================
gulp.task('webserver', function () {
    browserSync(config);
});

// ============== CLEAN =================
gulp.task('clean', function (cb) {
    rimraf(path.clean, cb);
});

// ============== DEFAULT RUN =================
gulp.task('default', ['build', 'webserver', 'watch']);
