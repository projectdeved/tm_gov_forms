$(document).ready(function () {
    $('.add-photo').on('click', function () {
        var counter = 0;
        var loader = $(this).parent().find('.loader');
        loader.addClass('show').text(counter + '%');

        var loader_timer = setInterval(function () {
            counter += 10;
            if (counter > 100) {
                clearInterval(loader_timer);
                loader.removeClass('show').text('');
            } else {
                loader.text(counter + '%');
            }
        }, 1000);

        return false;
    });

    $('#menu-trigger').on('click', function() {
        $('body').toggleClass('menu-opened');
        return false;
    });

    // Close menu on menu items click
    //$('.menu-anchor, .info-anchor').on('click', function() {
    //    $('body').removeClass('menu-opened');
    //});
});